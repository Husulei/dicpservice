<?php
/*
 * 模块: hydra_DICPNewsNav.php
 * 功能: 从化物所新闻主页新闻目录页面分离新闻出新闻地址列表
 * 主要接口:
 *      function NewsNavPage($url)
 *          初始化对象
 *      function get_all()
 *          完成新闻地址列表获取
 ******************************************************
 * 使用示例:
 * 1) 以新闻目录页地址初始化.
 * $url = "http://www.dicp.ac.cn/xwzx/tzgg/index_20.html";
 * $nav = new NewsNavPage($url);
 * 2) 完成内容分离.
 * $nav->get_all();
 * $nav结构将包含该目录页的新闻标题和地址列表.
 *
 */
class NewsNavPage
{
    var $url = "";      // 目录页地址
    var $url_base = ""; // 目录页最后一个`/`之后的*.html文件名
    var $url_dir = "";  // 目录页最后一个`/`之前的路径
    
    var $pre_url = "";  // `上一页`目录地址(当前为首页是, 此项为空)
    var $next_url = ""; // `下一页`目录地址(当前为最后一页, 此项为空)
    
    var $news_list = "";    // 新闻标题和地址列表
    
    var $dom = "";  // 存储当前目录页的DOM
    
    // exception info
    // 异常信息, 保留前一步操作后的状态
    var $ok = true;
    var $err = 0;
    var $warn = 0;
    
    function NewsNavPage($url)
    // 功能: 通过目录页地址初始化对象
    // 参数: $url, 目录页地址, 字符串;
    // 返回: $this;
    {
        $url = strtolower( trim($url) );
        
        $url_pattern = '/^http:\/\//';
        
        if (preg_match($url_pattern, $url))
        {
            $this->url = $url;
            $this->url_base = basename($url);
            $this->url_dir = dirname($url);
            
            $this->ok = true;
        }
        else 
        {
            $this->ok = false;
            $this->err = 1;
        }
        
        return $this;
    }// function NewsNavPage()
    
    
    function get_all()
    // 功能: 调用内部方法获取当前目录页的新闻列表
    // 参数: Null
    // 返回: $this
    {
        $this->init_dom();      // 初始化DOM
        $this->get_news_list(); // 获取新闻列表
        $this->get_neighbour(); // 获取相邻目录页地址
        
        return $this;
    }// function get_all()
    
    
    function init_dom()
    // 功能: 初始化DOM
    // 参数: Null
    // 返回: $this
    {
        // 调用DOMDocument方法
        // PHP官方文档: http://php.net/manual/en/class.domdocument.php
        $this->dom = new DOMDocument;
        $status = @$this->dom->loadHTMLFile($this->url);
        if (! $status)
        {// 初始化错误
            $this->ok = false;
            $this->err = 2;
        }
        else
        {// 初始化成功
            @$this->dom->normalizeDocument();
            $this->ok = true;
        }
        
        return $this;
    }// function init_dom()
    
    
    function get_news_list()
    // 功能: 获取新闻标题和地址列表
    // 参数: Null
    // 返回: $this
    {
        // use XPath
        // 调用DOMXPath方法
        // PHP官方文档: http://php.net/manual/en/class.domxpath.php
        // XPATH在线参考文档: http://www.w3school.com.cn/xpath/index.asp
        $finder = new DOMXPath($this->dom);
        $query_news = '//td[@class="hh14"]/a[@class="hh14"]';
	$query_date = '//td[@class="black12data"]';
        
        $result = $finder->query($query_news);
        $result_date = $finder->query($query_date);
		
        $a_list = array();
        if ($result)
        {   
            for ($i = 0; $i < $result->length; $i++)
            {
                $a = $result->item($i);
                $href = $a->getAttribute('href');
                $href_abs = $this->url_dir . '/' . $href;
                $href_std = $this->url_remove_dot($href_abs);
                
				//发布日期
		$dateattd = $result_date->item($i);
		$datetemp = $dateattd->nodeValue;
		$datest = substr($datetemp, 1, 10);
		//echo $datest;
                // 地址和标题以及发布时间
                $a_list[$i]['href'] = $href_std;
                $a_list[$i]['value'] = $a->nodeValue;
		$a_list[$i]['time'] = $datest;
		//echo $a_list[$i]['time']."position=".$i."  ";
            }
        }
        
        $this->news_list = $a_list;
        //echo $this->news_list[1]['date'];
        return $this;
    }// get_news_list()
    
    
    function get_neighbour()
    // 功能: 获取当前目录页的上一页和下一页源地址
    // 参数: Null
    // 返回: $this
    {
        // use XPath
        // 调用DOMXPath方法
        // PHP官方文档: http://php.net/manual/en/class.domxpath.php
        // XPATH在线参考文档: http://www.w3school.com.cn/xpath/index.asp
        $finder = new DOMXPath($this->dom);
        $pre = '//td/a[text()="上一页"]';
        $next = '//td/a[text()="下一页"]';
        
        // pre
        $result = $finder->query($pre);
        if ($result && $result->length > 0)
        {// 存在`上一页`
            $href = $result->item(0)->getAttribute('href');
            $this->pre_url = $this->url_dir . '/' . basename($href);            
        }
        
        // next
        $result = $finder->query($next);
        if ($result && $result->length > 0)
        {// 存在`下一页`
            $href = $result->item(0)->getAttribute('href');
            $this->next_url = $this->url_dir . '/' . basename($href);
        }
        
        return $this;
    }// function get_neighbour()
    
    
    function url_remove_dot( $path )
    // 功能: 移除超链接地址中的相对路径符号(`.`和`..`)
    // 参数: $path, 超链接路径, 字符串;
    // 返回: 处理之后的拆链接路径, 字符串;
    {   // 此段代码参考一下文章, 此处对$path作最简化假设处理:
        // http://nadeausoftware.com/articles/2008/05/php_tip_how_convert_relative_url_absolute_url
        // multi-byte character explode
        $url_pattern = '/^http:\/\//';
        $base = basename($path);
        $path = dirname($path);
        $path = substr($path, 6, strlen($path) - 6);
        
        $inSegs  = preg_split( '!/!u', $path );
        $outSegs = array( );
        foreach ( $inSegs as $seg )
        {
            if ( $seg == '' || $seg == '.')
                continue;
            if ( $seg == '..' )
                array_pop( $outSegs );
            else
                array_push( $outSegs, $seg );
        }
        $outPath = implode( '/', $outSegs );
        /*
        if ( $path[0] == '/' )
            $outPath = '/' . $outPath;
        // compare last multi-byte character against '/'
        if ( $outPath != '/' &&
                (mb_strlen($path)-1) == mb_strrpos( $path, '/', 'UTF-8' ) )
                    $outPath .= '/';
         */
        $outPath = "http://" . $outPath . "/" . $base;
        
        return $outPath;
    }
    
}//NewsNavPage

?>
