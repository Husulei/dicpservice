<?php
/*
 * 模块: update_newsdb_all.php
 * 功能: 更新左右栏目新闻数据库
 *      从第一个目录页遍历只最后一个目录页
 */
include 'hydra_DICPNewsMng.php';

$tzgg = "http://www.dicp.ac.cn/xwzx/tzgg/index.html";   // 通知公告
$xshd = "http://www.dicp.ac.cn/xwzx/xshd/index.html";   // 学术活动
$kjdt = "http://www.dicp.ac.cn/xwzx/kjdt/index.html";   // 科技动态
$zhxw = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";   // 综合新闻
// 各新闻栏目目录首页
$categ = array(
        'tzgg' => $tzgg,
        'xshd' => $xshd,
        'kjdt' => $kjdt,
        'zhxw' => $zhxw
);

// 创建数据库管理类型对象
$ndb = new NewsDB("localhost", "root", "dicp*mobile", "trial_db");
// 准备数据库
prepare_db($ndb);
// 更新新所有栏目新闻
update_db_all($ndb, $tzgg);

?>
