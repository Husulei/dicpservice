<?php

class NewsDB
{

// mysql account
    var $server = "localhost";
    var $user = "root";
    var $passwd = "";
    
    var $conn = "";
    
    var $DB = "";
    
    
    function NewsDB($s, $u, $p, $db)
    {
        $this->server = $s;
        $this->user = $u;
        $this->passwd = $p;
        $this->DB = $db;
    }// function NewsDB()
    
    function connect()
    {
        $this->$conn = @mysql_connect($this->server, $this->user, $this->passwd);
        
        return (! $this->$conn) ? false : true;
    }// function connect()
    
    
    function do_create_db()
    {
        $sql = "CREATE DATABASE " . $this->DB;
        
        $status = mysql_query($sql, $this->conn);
        
        return (! $status) ? false : true;
    }// function do_create_db()
    
    
    function init_db()
    {
        $db_list = mysql_list_dbs($this->conn);
        $cnt = mysql_num_rows($db_list);
        
        $status = false;
        for ($i = 0; $i < $cnt; $i++)
        {
            if (mysql_db_name($db_list, $i) == $this->DB)
            {
                $status = true;
                break;
            }
        }        
        
        if (! $status)
        {
            $status = $this->do_create_db();
        }
        
        $status = mysql_select_db($this->DB, $this->conn);
        
        if (! $status)
        {
            return false;
        }
        else 
        {
            mysql_query("set character set 'utf-8'");
            mysql_query("set names 'utf-8'");
            
            return true;
        }
    }// function init_db()
    
    
    function init_tbs()
    {
        // Stucture of news
        $sql_news = "
                CREATE TABLE news_tb
                (
                    news_id     int,
                    index       varchar(255)    UNIQUE,
                    category    varchar(8),
                    title       varchar(255),
                    time        date,
                    depart      varchar(255), 
                    source      varchar(255),
                    
                    PRIMARY KEY(news_id),
                )
                ";
        
        $sql_content = "
                CREATE TABLE content_tb
                (
                    para_id     int,
                    news_id     int,
                    para_order  int,
                    para        text,
                
                    PRIMARY KEY(para_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        
        $sql_pic = "
                CREATE TABLE pic_tb
                (
                    pic_id      int,
                    news_id     int,
                    pic_order   int,
                    pic_url     varchar(255),
                
                    PRIMARY KEY(pic_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        $sql_attach = "
                CREATE TABLE attach_tb
                (   
                    attach_id   int,
                    news_id     int,
                    attach_order int,
                    attach_url  varchar(255),
                
                    PRIMARY KEY(attach_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        
        $tbs = array(   
                "news_tb", 
                "content_tb",
                "pic_tb",
                "attach_tb"
                    );
        
        $sql = array(   
                $tbs[0] => $sql_news,
                $tbs[1] => $sql_content,
                $tbs[2] => $sql_pic,
                $tbs[3] => $sql_attach
                    );
        
        for ($i = 0; $i < 4; $i++)
        {
            // test if TABLE exists
            $sql_show = "SHOW TABLES LIKE " . $tbs[$i];
            $cnt = mysql_num_rows(mysql_query($sql_show, $this->conn));
            
            // if not exists, do create
            if($cnt < 1)
            {
                $status[$i] = mysql_query($sql[$tbs[$i]], $this->conn);
            }
            else
            {
                $status[$i] = true;
            }
        }
        
        return $status;
    }//function do_create_tb()

}

?>