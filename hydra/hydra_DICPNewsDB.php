<?php
/*
 * 模块: hydra_DICPNewsDB.php
 * 功能: 新闻数据库的链接, 存取, 查询
 * 主要接口:
 *      function NewsDB($s, $u, $p, $db)
 *          初始化数据库链接管理对象;
 *      function connect()
 *          连接数据库服务器;
 *      function init_db()
 *          初始化所用数据库文件, 若不存在则创建;
 *      function init_tbs()
 *          初始化所以数据表文件, 若不存在则创建;
 *      function insert_news($news)
 *          插入新闻条目;
 *      function delete_news($index)
 *          根据索引删除新闻条目;
 *      function news_of_index($index)
 *          根据索引查询新闻条目元数据;
 *      function cntnt_of_index($index)
 *          根据索引查询新闻条目内容;
 *      function news_of_categ($categ)
 *          根据新闻栏目类别查询新闻元数据并列表;
 *      function disconnect()
 *          断开与数据库服务器的连接;
 */
class NewsDB
{

    // mysql account
    // mysql数据库服务器账号信息
    var $server = "localhost";  // 主机
    var $user = "root"; // 用户名
    var $passwd = "";   // 密码
    
    // db connect
    var $Conn = ""; // mysqli数据库服务器连接对象
    
    // working db
    var $DB = "";   // 工作数据库文件名
    
    
    // exception info
    // 异常信息
    var $ok = true;
    var $err = 0;
    var $warn = 0;
    
    function NewsDB($s, $u, $p, $db)
    // 功能: 创建NewsDB类型对象
    // 参数: $s, 服务器; $u, 用户名; $p, 密码; $db, 工作数据库文件名;
    // 返回: $this
    {
        $this->server = $s;
        $this->user = $u;
        $this->passwd = $p;
        $this->DB = $db;
        
        return $this;
    }// function NewsDB()
    
    function connect()
    // 功能: 连接至数据库服务器
    // 参数: Null
    // 返回: true, 成功; false, 失败;
    {
        // 根据账号信息创建mysqli数据库服务器连接对象
        // PHP mysqli文档: http://php.net/manual/zh/class.mysqli.php
        $this->Conn = new mysqli($this->server, $this->user, $this->passwd);
        
        if (! $this->Conn)
        {// 连接失败
            $this->err = 1;
            return false;
        }
        else
        {// 连接成功
            // 设置客户端, 连接器编码
            $this->Conn->autocommit(true);
            $this->Conn->set_charset("utf8");
            //$this->Conn->query("set character set 'utf8'");
            $this->Conn->query("set names 'utf8'");
            return true;
        }
    }// function connect()
    
    
    function disconnect()
    // 功能: 断开数据库服务连接
    // 参数: Null
    // 返回: $this
    {
        $this->Conn->close();
        return $this;
    }
    
    
    function do_create_db($db)
    // 功能: 创建数据库文件
    // 参数: $db, 数据库文件名;
    // 返回: true, 成功; false, 失败;
    {
        // SQL查询语句
        $sql = "CREATE DATABASE " . $db;
        // 实施查询
        $status = $this->Conn->query($sql);
        
        if ($status)
        {// 创建成功
            return true;
        }
        else
        {// 创建失败
            return false;
        }
    }// function do_create_db()
    
    
    function do_db_exist($db)
    // 功能: 测试数据库文件是否存在
    // 参数: $db, 数据库文件名
    // 返回: true, 存在; false, 不存在;
    {
        // SQL查询语句;
        $sql = 'SHOW DATABASES LIKE "' . $db . '"';
        // 查询结果
        $db_list = $this->Conn->query($sql);
        
        if ($db_list->num_rows > 0)
        {// 查询结果非空, 表明存在
            return true;
        }
        else 
        {// 查询结果为空, 表明不存在
            return false;
        }
    }// function do_db_exist()
    
    
    function init_db()
    // 功能: 初始化数据库文件, 选择工作数据库文件, 若不存在, 则先创建;
    {
        if (! $this->do_db_exist($this->DB))
        {// 数据库文件不存在
            // 创建之
            $status = $this->do_create_db($this->DB);
            if (! $status)
            {// 创建不成功
                $this->err = 21;
                return false;
            }
        }
        
        // 选择数据库文件为工作数据库文件
        $this->Conn->select_db($this->DB);
        return true;
    }// function init_db()
    
    
    function do_tb_exist($tb)
    // 功能: 判断工作数据库中是否存在给定数据表
    // 参数: $tb, 数据表名;
    // 返回: true, 存在; false, 不存在;
    {
        // SQL查询
        $sql = 'SHOW TABLES LIKE "' . $tb . '"';
        $tb_list = $this->Conn->query($sql);
        
        if ($tb_list->num_rows > 0)
        {// 查询结果非空, 表明存在
            return true;
        }
        else
        {// 查询结果为空, 表明不存在
            return false;
        }
    }// function do_tb_exist($tb)
    
    
    function do_create_tb($query_tb)
    // 功能: 创建数据表
    // 参数: $query_tb, 创建数据表的SQL查询语句
    // 返回: Null;
    {
        $this->Conn->query($query_tb);
        $this->Conn->commit();
    }// function do_create_tb()
    
    
    function init_tbs()
    // 功能: 初始化存储新闻信息的数据表, 若不存在, 则先创建之
    // 参数: Null
    // 返回: true, 成功; false, 失败;
    {
        // 创建新闻元数据表的SQL查询
        $sql_news = "
                CREATE TABLE news_tb
                (
                    nid         int AUTO_INCREMENT,
                    nindex      varchar(256)    UNIQUE,
                    ncateg      varchar(16),
                    nurl        varchar(256),
                    ntitle      varchar(256),
                    ntime       date,
                    nauth       varchar(256),
                    
                    PRIMARY KEY(nid)
                )
                ";
        // 创建新闻内容数据表的SQL查询
        $sql_content = "
                CREATE TABLE content_tb
                (
                    cid         int AUTO_INCREMENT,
                    nindex      varchar(256),
                    content     text,
                    nimgs       text,
                
                    PRIMARY KEY(cid)
                )
                ";
        
        /*
        $sql_pic = "
                CREATE TABLE pic_tb
                (
                    pic_id      int,
                    news_id     int,
                    pic_order   int,
                    pic_url     varchar(255),
                
                    PRIMARY KEY(pic_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        $sql_attach = "
                CREATE TABLE attach_tb
                (   
                    attach_id   int,
                    news_id     int,
                    attach_order int,
                    attach_url  varchar(255),
                
                    PRIMARY KEY(attach_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        */
        // 所有数据表名称
        $tbs = array(   
                "news_tb", 
                "content_tb",
                    );
        // 所有数据表的创建查询
        $sql = array(   
                $tbs[0] => $sql_news,
                $tbs[1] => $sql_content,
                    );
        
        // 关闭自动提交, 开启事务查询
        $this->Conn->autocommit(false);
        for ($i = 0; $i < 2; $i++)
        {
            // 判断数据表是否存在
            if (! $this->do_tb_exist($tbs[$i]))
            {// 不存在
                // 创建数据表
                $this->Conn->query($sql[$tbs[$i]]);
            }
        }
        
        if (! $this->Conn->errno)
        {// 查询无异常
            // 提交事务
            $this->Conn->commit();
            $this->Conn->autocommit(true);
            return true;
        }
        else 
        {// 查询有异常
            // 事务回滚
            $this->Conn->rollbak();
            $this->Conn->autocommit(true);
            // 设置错误信息
            $this->err = 22;
            return false;
        }        
        
    }//function do_init_tbs()
    
    
    function insert_news($news)
    // 功能: 向数据库插入新闻条目
    // 参数: $news, News对象;
    // 返回: true, 成功; false, 失败;
    {   
        // INSURE THAT: $news is an object (defined in 'hydra_DICPNews.php')
        if (! (gettype($news) == 'object' && get_class($news) == 'News') )
        {// $news非News类型对象
            $this->err = 31;
            return false;
        }

        // escape special characters
        /**************************************************************
        $index  = $this->Conn->real_escape_string($news->index);
        $categ  = $this->Conn->real_escape_string($news->categ_t);
        $url    = $this->Conn->real_escape_string($news->src_url);
        $title  = $this->Conn->real_escape_string($news->title);
        $time   = $this->Conn->real_escape_string($news->date_std);
        $auth   = $this->Conn->real_escape_string($news->department);
        $cntnt  = $this->Conn->real_escape_string($news->content_t);
        $imgs   = $this->Conn->real_escape_string($news->images_t);
        ***************************************************************/
        
        $index  = $news->index;
        $categ  = $news->categ_t;
        $url    = $news->src_url;
	
        $title  = $news->title;
	//echo "title";
        $time   = $news->date_std;
	$timett = new DateTime($time);
	//echo $timett->format('Y-m-d')."DB";
	//echo $index."indexDB";
	//echo $new->date_std;
        $auth   = $news->department;
        $cntnt  = $news->content_t;
        $imgs   = $news->images_t;
 
        // remove non-breaking space: do not work
        //$cntnt = str_replace("\xa0",'', $cntnt);

        // DEBUG MODE
        //print_r($cntnt); echo "<br/>";

        // SQL插入查询语句
        $sql_news = "
                INSERT INTO news_tb 
                (nindex, ncateg, nurl, ntitle, ntime, nauth)
                VALUES
                ('$index', '$categ', '$url', '$title', '$time', '$auth')
                ";
        $sql_cntnt = "
                INSERT INTO content_tb
                (nindex, content, nimgs)
                VALUES
                ('$index', '$cntnt', '$imgs')
                ";
        // 开启事务模式, 保证元数据与内容一致
        $this->Conn->autocommit(false);
        $this->Conn->query($sql_news);  // 插入元数据
        $this->Conn->query($sql_cntnt); // 插入内容
        if (! $this->Conn->errno)
        {// 无异常
            // 提交事务
            $this->Conn->commit();
            return true;
        }
        else
        {// 有异常
            // 事务回滚
            $this->Conn->rollback();
            $this->err = 32;
            return false;
        }
        
    }// function insert_news()
    
    
    function delete_news($index)
    // 功能: 删除新闻条目
    // 参数: $index, 待删新闻索引;
    // 返回: true, 成功; false, 失败;
    {
        // SQL查询语句
        $sql_news = "
                DELETE FROM trial_db WHERE nindex='$index'
                ";
        $sql_cntnt = "
                DELETE FROM content_tb WHERE nindex='$index'
                ";
        
        /*
        $sql_news = "
                DELETE content_tb, trial_db
                FROM content_tb, trial_db 
                WHERE 
                    content_tb.nindex='$index'
                    AND
                    trial_db.nindex='$index'
                ";
        */
        
        // 开启事务模式
        $this->Conn->autocommit(false);
        $this->Conn->query($sql_cntnt); // 删除内容
        $this->Conn->query($sql_news);  // 删除元数据
        if (! $this->Conn->errno)
        {
            // 提交事务
            $this->Conn->commit();
            return true;
        }
        else
        {
            // 事务回滚
            $this->Conn->rollback();
            $this->err = 33;
            return false;
        }
        
    }// function delete_news()
    
    
    function news_of_index($index)
    // 功能: 查询给定索引新闻的元数据
    // 参数: $index, 新闻索引;
    // 返回: 查询成功则以关联数组返回查询结果; 失败则返回false; 
    {
        // SQL 查询语句
        $sql = "SELECT * FROM news_tb WHERE nindex='$index'";
        $result = $this->Conn->query($sql);
        
        if ($result)
        {// 查询成功
            // 以关联数组形式返回
            $row = $result->fetch_assoc();
            return $row;
        }
        else 
        {// 查询失败
            $this->err = 41;
            return false;
        }
    }// function news_of_index()
    
    
    function cntnt_of_index($index)
    // 功能: 查询给定索引新闻的内容
    // 参数: $index, 新闻索引;
    // 返回: 查询成功则以关联数组返回查询结果; 失败则返回false; 
    {
        // SQL 查询语句
        $sql = "SELECT * FROM content_tb WHERE nindex='$index'";
        $result = $this->Conn->query($sql);
        
        if ( $result )
        {// 查询成功
            // 以关联数组形式返回
            $row = $result->fetch_assoc();
            return $row;
        }
        else 
        {// 查询失败
            $this->err = 41;
            return false;
        }
    }// function cntnt_of_index()
    
    
    function news_of_categ($categ)
    // 功能: 查询给定栏目类别的新闻(元数据)列表
    // 参数: $categ, 新闻栏目名称;
    // 返回: 查询成功则以关联数组返回查询结果; 失败则返回false;
    {
        // SQL 查询
        $sql = "
            SELECT * FROM news_tb 
            WHERE ncateg='$categ'
            ORDER BY ntime DESC
            ";
        
        $result = $this->Conn->query($sql);
        if ( $result )
        {// 查询成功
            // 以数组返回所有查询结果
            $news = array();
            $i = 0;
            while ($row = $result->fetch_assoc())
            {
                $news[$i] = $row;
                $i++;
            }
            return $news;
        }
        else
        {// 查询失败
            $this->err = 41;
            return false;
        }//if
    }// function news_of_categ()

}// class NewsDB

?>
