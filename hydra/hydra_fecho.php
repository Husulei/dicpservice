<?php


class Hydra_fecho
{
    var $FECHO_FILE = "";
    var $FECHO_FD = "";
    
    function Hydra_fecho($file)
    {
        $this->FECHO_FILE = $file;
        $this->FECHO_FD = fopen($this->FECHO_FILE, "a+");
        return $this;
    }

    function xecho($text)
    {
        fwrite($this->FECHO_FD, $text);
        return $this;
    }
    
        
    function xclose()
    {
        fclose($this->FECHO_FD);
        return $this;
    }
    
    function xclear()
    {
        ftruncate($this->FECHO_FD, 0);
        return $this;
    }
    
    function endl()
    {
        return $this->xecho("\n");
    }
    
}//end class

$FEcho = new Hydra_fecho('hydra_fecho_text.txt');

?>