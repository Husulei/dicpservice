<?php
header("Content-Type: text/html; charset=utf-8");
/*
 * 模块: hydra_DICPNews.php
 * 功能: 从化物所新闻主页单条新闻页面分离新闻内容以备存储数据库
 * 主要接口:
 *      function News($url)
 *          初始化对象
 *      function get_all()
 *          完成新闻内容分离
 ******************************************************
 * 使用示例:
 * 1) 以新闻地址初始化.
 * $url = 'http://www.dicp.ac.cn/xwzx/tzgg/201407/t20140729_4168995.html';
 * $news = new News($url);
 * 2) 完成内容分离.
 * $news->get_all();
 * $news结构将包含新闻内容与元数据.
 *  
 */
class News
{
    var $src_url = "";  //新闻源地址
    var $src_base = ""; //源地址最后一个`/`之后的*.html文件名
    var $src_dir = "";  //源地址最后一个`/`之前的路径
    
    var $dom = "";      //以新闻源文件初始化的DOM文件
                        //DOM参考文档:
                        //http://php.net/manual/en/class.domelement.php
    // mata datas
    var $categ_t = "";  //新闻的类别字符串标记
    var $categ_n = "";  //新闻类别数字标记, 由$categ2n定义
    
    var $index = "";    //生成的新闻唯一标示符
    var $date = "";     //新闻发布日期
    var $date_std = ""; //发布日期的标准格式
    
    var $title = "";    //新闻标题
    var $department = "";   //发稿部门
    
    var $content_dom = "";  //存储新闻内容的DOM
    //var $content_div = "";
    var $content_t = "";    //存储新闻内容的字符串(保留特定html标签, 由$reserved定义)
    
    var $images = "";       //图片地址列表
    var $images_t = "";     //字符串化地址列表
    var $attachs = "";      //[未实现]附件列表
    
    // category of news
    var $categ2n = array(   
            'zhxw' => 11,
            'kjdt' => 12,
            'xshd' => 13,
            'tzgg' => 14,
	    'xstz/xsbg' => 15,
	    'xstz/lwdb' => 16
            );  //新闻类别数字标记定义
    var $n2categ = array(   
            11 => 'zhxw',
            12 => 'kjdt',
            13 => 'xshd',
            14 => 'tzgg',
	    15 => 'xstz/xsbg',
	    16 => 'xstz/lwdb'
            );  //新闻类别数字标记定义
    
    // reserved tag in content of news
    var $reserved = array(  
            'tag' => array('div', 'p', 'a'),
            'attr' => array('href', 'src')
            );  //新闻内容中保留的标签列表
    
    // 调试信息, 保留前一步操作之后的状态, 
    var $ok = true; //操作成功
    var $err = 0;   //产生错误
    var $warn = 0;  //产生警告
    var $n = 0;     
    var $dateTemp = ""; 
    function News($url)
    //功能: 通过新闻源地址初始化对象
    //参数: $url, 新闻源地址
    //返回: 该对象
    {	
	//	echo $url;
	//echo $newdate;
	//$this->dateTemp = trim($newdate);
	//$date_format = "%Y%m%d";
        //echo "newdate".trim($newdate);
        //$date['Y'] = substr($newdate, 0, 4);
        //$date['m'] = substr($newdate, 5, 2);
        //$date['d'] = substr($newdate, 7, 2);
        //$this->date = $date;
        //$this->date_std = date("Y-m-d", mktime(0, 0, 0, $date['m'], $date['d'], $date['Y']));
	//echo $date_std;
        $url = strtolower( trim($url) );
	//$this->dateTemp = $newdate;
        $url_pattern = '/^http:\/\//';        
        if (preg_match($url_pattern, $url))
        {
            $this->src_url = $url;
            $this->src_base = basename($url);
            $this->src_dir = dirname($url);
            
            $this->ok = true;
        }
        else 
        {
            $this->ok = false;
            $this->err = 1;
        }
        
        return $this;
    }// function News()
    
    function setDate($newsdate) {
        $this->dateTemp = trim($newsdate);
	//echo $this->dateTemp;
    }

    
    function get_all()
    //功能: 调用内部方法完成新闻内容分离
    //参数: 无
    //返回: 该对象
    {
        $this->matainfo();  //获取元数据
        $this->init_dom();  //初始化DOM
        $this->get_title(); //获取标题
        $this->get_department();    //获取新闻发布单位
        $this->get_content_dom();   //获取新闻内容DOM
        $this->clean_content_dom(); //过滤新闻内容DOM内的无用信息
        $this->list_img();  //获取图片列表
        $this->get_content_t(); //获取新闻内容文本(保留特定标签)
        
        return $this;   //返回
    }// function get_all();
    
    
    function matainfo()
    //功能: 获取元数据
    //参数: 无
    //返回: $this
    {
        // parse full url into seperate directory level
        // 将新闻地址按`/`解析
        $path = substr($this->src_url, 7);
        $path_a = explode('/', $path);
        $depth = count($path_a);
        
        // get category
        // 获取新闻类别
        $categ = $path_a[2];
        //! for nesting category 
        //! defined as "xstz/xsbg"
        //! 有可能存在类似"xstz/xsbg"的嵌套类别
        $categ_pattern = '/^[a-z]+$/';
        for ($i = 3; $i < $depth; $i++)
        {
            if (preg_match($categ_pattern, $path_a[$i]))
            {
                $categ = $categ . '/' . $path_a[$i];
            }
            else 
            {
                break;
            }
        }
        // category in text
        $this->categ_t = $categ;
	//echo $this->categ_t;
        // category in enumeration
        if (array_key_exists($categ, $this->categ2n))
        {
            $this->categ_n = $this->categ2n[$categ];
        }
        else
        {// category DO NOT included in current consideration
         // 对于暂时未定义的新闻类别数字标记, 归为99
            $this->categ_n = 99;
            $this->warn = 1;
        }
        
        // get *.html file name
        // 获取新闻html文件名
        $htm = $path_a[$depth - 1];
        $basename = substr($htm, 1, strlen($htm) - 6);
	//echo $basename;
        $basename_a = explode('_', $basename);
        //echo $basename_a[0];
        // get date
        // 获取日期
        //$date_text = $basename_a[0];
        $date_format = "%Y%m%d";
	//echo $this->dateTemp;
        $date['Y'] = substr($this->dateTemp, 0, 4);
        $date['m'] = substr($this->dateTemp, 5, 2);
        $date['d'] = substr($this->dateTemp, 8, 2);
        $this->date = $date;
	//echo $date['Y'];
        $this->date_std = date("Y-m-d", mktime(0, 0, 0, $date['m'], $date['d'], $date['Y']));
	//echo $date['Y']."dateY";
	//echo $date['m']."datem";
	//echo $date['d']."dated";
       // echo $this->dateTemp."dateTemp";
	$timetta = new DateTime($this->date_std);
        //echo $timetta->format('Y-m-d')."News";
	//$this->date_std = $newdate;
        // generate index
        // 根据类别与日期生成唯一的新闻索引
        $this->index = "$this->categ_n" . '_' . $basename;
        //echo $this->index."index";
        return $this;
    }// function matainfo()
    
    function init_dom()
    //功能: 初始化DOM
    //参数: Null
    //返回: $this
    {
        // 调用DOMDocument方法
        // PHP官方文档: http://php.net/manual/en/class.domdocument.php
        $this->dom = new DOMDocument;
        $status = @$this->dom->loadHTMLFile($this->src_url);
        if (! $status)
        {// 初始化错误
            $this->ok = false;
            $this->err = 2;
        }
        else 
        {// 初始化成功
            @$this->dom->normalizeDocument();
            $this->ok = true;
        }
        
        return $this;
    }//function init_dom()
    
    
    function get_title()
    //功能: 获取标题
    //参数: Null
    //返回: $this
    {	
        // use XPath
        // 调用DOMXPath方法
        // PHP官方文档: http://php.net/manual/en/class.domxpath.php
        // XPATH在线参考文档: http://www.w3school.com.cn/xpath/index.asp
        $finder = new DOMXPath($this->dom);
	
	if ($this->categ_t == "xstz/xsbg") {
	    $query_title = '//table[@class = "h14l24"]/tbody/tr/td[@width = "100%"]';
	    $title_xml = $finder->query($query_title)->item(0);
	    //echo $title_xml->textContent;
        } else {
            // 通过以下特定属性定位标题
            $query_title = '//table[@class = "blu23l28"]/tbody/tr/td[@width = "100%"]';
	    $title_xml = $finder->query($query_title)->item(0);
            //echo $title_xml->textContent;
	}

        //$title_xml = $finder->query($query_title)->item(0);
        
        // 获取标题文本
        $this->title = $title_xml->textContent;
        
        // trim useless white space
        trim($this->title);
        //echo $this->title;
        return $this;
    }// function get_title()
    
    
    function set_title($title)
    //功能: 设定新闻标题
    //参数: $title, 字符串, 待设定新闻标题
    //返回: $this;
    {// possiblely be used before fetch and parse *.htm file
        $this->title = $title;
        return $this;
    }// function set_title()
    

    function get_department()
    //功能: 获取新闻发布单位
    //参数: Null
    //返回: $this
    {
        // use XPath
        // 调用DOMXPath方法
        // PHP官方文档: http://php.net/manual/en/class.domxpath.php
        // XPATH在线参考文档: http://www.w3school.com.cn/xpath/index.asp
        $finder = new DOMXPath($this->dom);
        // 定位
        $query_depart = '//span[@id = "auth"]';
        
        $depart_anchor = $finder->query($query_depart);
        if ($depart_anchor->length > 0)
        {
            $depart_anchor = $finder->query($query_depart)->item(0);
            $depart_name = $depart_anchor->nextSibling;
            if ($depart_name)
            {
                $this->department = $depart_name->data;
                trim($this->department);
            }
        }
        
        return $this;
    }// function get_department()
    
    
    function get_content_dom()
    //功能: 获取新闻内容DOM
    //参数: Null
    //返回: $this
    {
        // XPath query
        // 调用DOMXPath方法
        // PHP官方文档: http://php.net/manual/en/class.domxpath.php
        // XPATH在线参考文档: http://www.w3school.com.cn/xpath/index.asp
        $finder = new DOMXPath($this->dom);
        
        //$query_content = '//div[@class = "TRS_Editor"][p or font or div or span]';
        //$query_content = '//div[@class = "TRS_Editor" or @class = "cas_content"]'
        //$query_content = '//td[@class = "h14124"][p]';
        // 定位新闻内容的查询列表
        // 沿此列表依次查询, 查到为止, 因此此处顺序很重要
        // ! 注意:
        // ! 新闻页面结构的变化将导致此处实现的繁琐
        // ! 请关注特殊结构新闻页面
        $query = array(
                '//div[@class = "TRS_Editor" or @class = "cas_content"][p]',
                '//*[p]',
                '//div[@class = "TRS_Editor" or @class = "cas_content"]'
                );
/***************************************************************/
        for ($i = 0; $i < count($query); $i++)
        {
            $result = $finder->query($query[$i]);
            if ($result->length > 0)
            {
                $content = $result->item(0);
                $this->content_dom = new DOMDocument;
                $this->content_dom->loadXML($content->C14N());
                
                // 查到即返回
                return $this;
            }            
        }
/**************************************************************/
/***************************************************************
        for ($i = 0; $i < count($query); $i++)
        {
            $result = $finder->query($query[$i]);

            $content = "";
            for ($j = 0; $j < $result->length; $j++)
            {
                $content .= $result->item($j)->C14N();
            }
            
            if ($content)
            {
                $this->content_dom = new DOMDocument;
                $this->content_dom->loadXML($content);
                return $this;
            }
        }
**************************************************************/
        
        //! 执行到此处表明前面定位内容DOM不成功, 设置错误代码
        $this->err = 11;
        return $this;
        /* discarding method
        $paras = '';
        foreach ($content->getElementsByTagName('p') as $p)
        {
            $p->removeAttribute('align');
            $p->removeAttribute('style');
            $paras = $paras . $p->C14N() . "\n";
        }
        */
        
    }// function get_content()
    
    
    function clean_content_dom()
    //功能: 清理内容DOM
    //参数: Null
    //返回: $this
    {
        // 主要清理超链接(相对链接)和无意义标记
        //$this->do_clean_imgsrc($this->content_dom);
        //$this->do_clean_ahref($this->content_dom);
        $this->do_clean_hyperlink($this->content_dom);
        $this->do_clean_node($this->content_dom);
        
        return $this;
    }
    
    
    function get_content_t()
    //功能: 获取新闻内容文本
    //参数: Null
    //返回: $this
    {
        $this->content_t = '';
        
        $children = $this->content_dom->childNodes;
        for ($i = 0; $i < $children->length; $i++)
        {
            $this->content_t = $this->content_t . $children->item($i)->C14N();
        }
        
        return $this;
    }// function get_content_t()
    

    function do_clean_node($node)
    //功能: 递归清理DOM节点标签(清理无用标签)
    //参数: $node, DOM节点
    //返回: true, 清理成功; false, 清理失败;
    {
        /* maxmum depth for recursive call
         * for test use
        if ($this->n > 20)
        {
            return false;
        }
        */
        
        // 先判断是否嵌套节点
        if (! $this->is_nest($node))
        {// 不是嵌套节点
            // 判断是否文本节点
            if (! $this->is_text($node))
            {// 非文本非嵌套节点
                // 清理顶层节点标签
                $this->do_clean_top($node);
            }
            
            // 返回
            return true;
        }
        else 
        {// 嵌套节点
            //$this->n ++;
            
            // 遍历其子节点
            $children = $node->childNodes;
            for ($i = 0; $i < $children->length; $i++)
            {
                // 递归清理子节点标签
                $this->do_clean_node($children->item($i));
            }
            
            //$this->n --;            
            // 清理顶层节点标签
            $this->do_clean_top($node);
            return true;
        }
    }

    
    function do_clean_top($node)
    //功能: 清理顶层节点标签
    //参数: DOM节点
    //返回: ture, 成功; false, 失败;
    {
        // 是否保留标签?
        $info = $this->is_reserved($node);
        if (! $info)
        {// 否
            // 展开标签内容
            $this->do_expand_top($node);
            return true;
        }
        else
        {// 是
            // 重新规范化顶层节点标签
            $this->do_renorm_top($node, $info);
            return true;
        }
    }
    
    
    function do_expand_top($node)
    //功能: 展开顶层节点标签
    //参数: DOM节点
    //返回: true, 成功; false, 失败;
    {
        // in case for root node
        // becasue insertBefore() is invalid
        $parent = $node->parentNode;
        if (! $parent)
        {// 根节点
            return true;
        }
                
        // 将子节点依次插到当前$node之前
        $children = $node->childNodes;
        for ($i = 0; $i < $children->length; $i++)
        {
            $child = $children->item($i)->cloneNode(true);
            $parent->insertBefore($child, $node);
        }
        
        // 删除$node
        $parent->removeChild($node);
        
        return true;
    }// function do_clean_top()
    
    
    function do_renorm_top($node, $info)
    //功能: 按给定信息重新规范化节点
    //参数: $node, DOM节点; $info, 节点信息;
    //返回: true, 成功; false, 失败;
    {
        // in case for root node
        // becase appendChild() is invalid
        $parent = $node->parentNode;
        if (! $parent)
        {// 根节点
            return true;
        }
        
        if($info)
        {
            // 重新定义标签名跟属性并依次插入子节点, 替换当前节点
            $root = $node->ownerDocument;
            $newnode = $root->createElement($info['name']);
            if(count($info['attr'])>0)
            {
                foreach (array_keys($info['attr']) as $attr_name)
                {
                    $attr_value = $info['attr'][$attr_name];
                    $newnode->setAttribute($attr_name, $attr_value);
                }
            }
            
            $children = $node->childNodes;
            for ($i = 0; $i < $children->length; $i++)
            {
                $child = $children->item($i)->cloneNode(true);
                $newnode->appendChild($child);
            }
            
            $parent->insertBefore($newnode, $node);
            $parent->removeChild($node);
            
            return true;
        }
        
    }// function do_renorm_top()

    
    function is_nest($node)
    //功能: 判断节点是否有嵌套
    //参数: $node, DOM节点
    //返回: true, 是; false, 否;
    {
        if ($node->hasChildNodes())
        {
            foreach ($node->childNodes as $child)
            {
                if ($child->nodeType == XML_ELEMENT_NODE)
                {
                    return true;
                }
            }
        }
        else 
        {
            return false;
        }
    }// function is_nest();
    
    
    function is_text($node)
    //功能: 判断节点是否文本节点
    //参数: $node, DOM节点
    //返回: true, 是; false, 否;
    {
        if ($node->nodeType == XML_TEXT_NODE)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }// function is_text()
    
    
    function is_reserved($node)
    //功能: 判断节点标签是否为保留标签
    //参数: $node, DOM节点
    //返回: false, 不是; $info结构, 保留标签的保留信息, 包括标签名和属性列表;
    {
        // in case for root node
        // because getAttribute() is invalid
        if ($node->nodeType == XML_DOCUMENT_NODE)
        {
            return false;
        }
        
        $nodeName = "";
        $nodeAttr = array();
        $result = array();
        
        // 查询节点是否含有保留属性
        foreach ($this->reserved['attr'] as $attr_name)
        {
            if ($attr_value = $node->getAttribute($attr_name))
            {
                $nodeAttr[$attr_name] = $attr_value;
            }
        }
        
        $is_rsrv_attr = count($nodeAttr);
        $is_rsrv_tag = in_array($node->nodeName, $this->reserved['tag']);
        if ($is_rsrv_attr || $is_rsrv_tag)
        {
            $nodeName = $node->nodeName;
            $result = array(
                'name' => $nodeName,
                'attr' => $nodeAttr
                );

            return $result;
        }
        else 
        {
            return false;
        }
    }// function is_reserved()
    
    
    function do_clean_hyperlink($dom)
    //功能: 清理新闻内容中的超链接(相对地址转为绝对地址)
    //参数: $dom, DOM文档;
    //返回: true, 成功; false, 失败;
    {// a uniform method to clean all possible kind of hyperlinks
     // and extensible
        $link_types = array(
                        'src',
                        'href'
                        );
        
        $finder = new DOMXPath($dom);
        
        foreach ($link_types as $type)
        {
            $query = '//*[@' . $type . ']';
            
            $links = $finder->query($query);
            
            for ($i = 0; $i < $links->length; $i++)
            {
                $link = $links->item($i);
                $src = $link->getAttribute($type);
                $src_abs = $this->do_format_url($src);
                $link->setAttribute($type, $src_abs);
            }
        }
        return true;
    }
    
    
    // too specific
    function do_clean_imgsrc($dom)
    //功能: 清理新闻内容中的图片超链接(相对地址转为绝对地址)
    //参数: $dom, DOM文档;
    //返回: true, 成功; false, 失败;
    {
        $finder = new DOMXPath($dom);
        $query_img = '//img';
        
        $imgs = $finder->query($query_img);
        
        for ($i = 0; $i < $imgs->length; $i++)
        {
            $img = $imgs->item($i);
            $src = $img->getAttribute("src");
            $src_abs = $this->do_format_url($src);
            $img->setAttribute("src", $src_abs);
        }
        
        return true;
    }// function do_clean_imgsrc()
    
    
    // too specific
    function do_clean_ahref($dom)
    //功能: 清理新闻内容中的<a></a>超链接(相对地址转为绝对地址)
    //参数: $dom, DOM文档;
    //返回: true, 成功; false, 失败;
    {
        $finder = new DOMXPath($dom);
        $query_a = '//a';
        
        $anchors = $finder->query($query_a);
        
        for ($i = 0; $i < $anchors->length; $i++)
        {
            $a = $anchors->item($i);
            $href = $a->getAttribute("href");
            $href_abs = $this->do_format_url($href);
            $a->setAttribute('href', $href_abs);
        }
        
        return true;
    }// function do_clean_ahref()
    
    
    function do_format_url($url)
    //功能: 将给定地址格式华为绝对地址
    //参数: $url, 地址字符串
    //返回: 转变后的地址字符串
    {
        if (! $this->is_absurl($url))
        {
            $url = $this->src_dir . '/' . $url;
        }
        
        return $url;
    }// function do_format_url();
    
    
    function is_absurl($url)
    //功能: 判断给定地址是否绝对地址
    //参数: $url, 地址字符串
    //返回: true, 是; false, 否;
    {
        $url = strtolower( trim($url) );
        
        $url_pattern = '/^http:\/\//';
        if (preg_match($url_pattern, $url))
        {
            return true;
        }
        else
        {
            return false;
        }        
    }// function is_absurl()
    
    
    function list_img()
    //功能: 获取图片地址列表
    //参数: Null
    //返回: $this;
    {
        // 调用DOMXPath方法
        // PHP官方文档: http://php.net/manual/en/class.domxpath.php
        // XPATH在线参考文档: http://www.w3school.com.cn/xpath/index.asp
        $finder = new DOMXPath($this->content_dom);
        $query_img = '//img';
        
        $imgnodes = $finder->query($query_img);
        $imgs = array();
        $this->images_t = '';
        for ($i = 0; $i < $imgnodes->length; $i++)
        {
            $imgs[$i] = $imgnodes->item($i)->getAttribute('src');
            $this->images_t = $this->images_t . "<src>" . $imgs[$i] . "</src>";
        }
        
        $this->images = $imgs;
        $this->images_t = $this->images_t;
        return $this;
    }
    
    /* TODO
        function fetch_attach()
    {
        return $this;
    }
    
    
    */
    
    
    function clear_err()
    //功能: 清除错误状态
    //参数: Null
    //返回: $this
    {
        $this->ok = true;
        $this->err = 0;
        $this->warn = 0;
        
        return $this;
    }
}


?>
