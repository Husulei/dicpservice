<?php
/******************************************************************************
**********************************DEBUG MODE***********************************
//error_reporting(E_ALL);
//ini_set("display_errors","On");
******************************************************************************/

header("Content-Type: text/html; charset=utf8");
include 'hydra_DICPNewsMng.php';




$ndb = new NewsDB("localhost", "root", "dicp*mobile", "trial_db");

//for debug: localhost
//$ndb = new NewsDB("localhost", "root", "", "trial_db");
prepare_db($ndb);

$tzgg = "http://www.dicp.ac.cn/xwzx/tzgg/index.html";
$xshd = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";
$kjdt = "http://www.dicp.ac.cn/xwzx/kjdt/index.html";
$zhxw = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";
$xsbg = "http://www.dicp.ac.cn/xwzx/xstz/xsbg/index.html";
$lwdb = "http://www.dicp.ac.cn/xwzx/xstz/lwdb/index.html";
$categ = array(
        'tzgg' => $tzgg,
        'xshd' => $xshd,
        'kjdt' => $kjdt,
        'zhxw' => $zhxw,
	'xstz/xsbg' => $xsbg,
        'xstz/lwdb' => $lwdb
        );
//echo "good";
// for debug, use $_GET
$q=$_POST['q'];

$filepath = "/var/www/html/mobile/countlogin.txt";
$fr = fopen($filepath, "r");
$oldn = fread($fr, filesize($filepath));
$newn = $oldn + 1;
fclose($fr);
$fo = fopen($filepath, "w");
fwrite($fo, $newn);
fclose($fo);


$refreshMode=$_POST['refreshMode'];  //更新模式，共三种，第一次获取列表(null)，上拉显示更多(showMore)，下拉更新(refresh)
$beginIndex=$_POST['beginIndex']; //在后两种模式中列表当前位置（上拉对应的列表最底端的项目的index编号，下拉对应列表最顶端的项目编号）
$contentNumber=(int)$_POST['contentNumber']; //每次更新或显示更多所返回的条目数目

if (array_key_exists($q, $categ)) //判断需要返回的新闻种类，总四种，通知公告，学术活动，科技动态，综合新闻
{
    if ($refreshMode == "null") //判断是否更新模式为第一次获取列表
    {
        $result = $ndb->news_of_categ($q); //从数据库返回所有的列表信息
        //print_r($result);
        if($contentContent < count($result)) //判断是否所需要显示的项目总数小于列表中项目总数
	{
            for ($i = 0; $i < $contentNumber; $i++) //循环从第一项开始直到每次显示数目条项目为止，依然将列表信息返回
            {
                echo $result[$i]['ntitle'] . "*"; //返回新闻标题
                echo $result[$i]['nauth'] . "*"; //返回新闻的作者
                echo $result[$i]['ntime'] . "*"; //返回新闻发布的时间
                echo $result[$i]['nurl'] . "*"; //返回新闻内容页面中图片对应的网页地址
                echo $result[$i]['nindex'] . "$";  //返回当前新闻条目的唯一标识符
	    }
        }
    } elseif($refreshMode == "showMore") //判断是否更新模式为显示更多
    {
	$tempnum = 0; //保存新闻起始条目在数据库中的数字编号的中间变量
	$result = $ndb->news_of_categ($q);  //从数据库返回所有的列表信息
	for ($i =0; $i < count($result); $i++) //循环从第一条直至最后一条
	{
	    if ($result[$i]['nindex'] == $beginIndex) //如果当前新闻条目的唯一标识符与请求信息中请求的索引相同，则将中间变量值赋为当前数字编号
	    {
		$tempnum = $i;	
		break;
	    }
	}
	
	if ($tempnum >= count($result)-1) //判断是否当前数字编号为新闻条目的最后一条新闻，如果是，则返回"nomore"，表示已经更多可以显示的新闻条目
	{
	    echo "nomore";
	}
	else //否则返回最多为请求数目的新闻条目
	{
	    for ($i = $tempnum+1; $i <= $tempnum+$contentNumber && $i < count($result); $i++) //从请求条目编号的下一条开始，直至最多为请求数目的新闻条目为止（不能超出列表的最大值）
            {
                echo $result[$i]['ntitle'] . "*";
                echo $result[$i]['nauth'] . "*";
                echo $result[$i]['ntime'] . "*";
                echo $result[$i]['nurl'] . "*";
                echo $result[$i]['nindex'] . "$";
            }
	}
    } elseif($refreshMode == "refresh") //判断是否更新模式为更新
    {
	$tempnum = 0;
        $result = $ndb->news_of_categ($q);
        for ($i =0; $i < count($result); $i++)
        {
            if ($result[$i]['nindex'] == $beginIndex)
            {
                $tempnum = $i;
		break;
            }
        }

        if ($tempnum <= 0) //如果请求更新条目的起始条目已经是当前的第一条信息，则表明当前数据库中尚无更新，则返回"norefresh"
        {
            echo "norefresh";
        }
        else
        {
            for ($i = $tempnum-1; $i >= $tempnum-$contentNumber && $i > -1; $i--) //否则从请求条目编码的下一条开始，直至最多为请求更新数目的条目对应的条目为止，返回信息
            {
                echo $result[$i]['ntitle'] . "*";
                echo $result[$i]['nauth'] . "*";
                echo $result[$i]['ntime'] . "*";
                echo $result[$i]['nurl'] . "*";
                echo $result[$i]['nindex'] . "$";
            }
        }
    }
}


$db=mysql_connect("localhost","root","dicp*mobile");
mysql_select_db("accesscount",$db);
//mysql_query('update count set '.$q.'='.$q.'+1');
if($q=="xstz/xsbg") {
    $q="xsbg";
}
mysql_query('update count set '.$q.'='.$q.'+1');

$checkresult = mysql_query('select * from newcount order by id desc limit 1');
$dayresult = mysql_fetch_array($checkresult);
$index = $dayresult['id'];
$day = $dayresult['day'];
$time = date('Y-m-d',time());
if ($day == $time) {
    mysql_query('update newcount set '.$q.'='.$q.'+1 where id='.$index);
} else { 
    mysql_query('insert into newcount (day, '.$q.') values (curdate(), 1)');
}

mysql_close();




?>
