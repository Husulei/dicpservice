<?php
/*
 * 模块: hydra_DICPNewsMng.php
 * 功能: 管理新闻数据库的更新
 * 依赖: hydra_DICPNews.php
 *      hydra_DICPNewsNav.php
 *      hydra_DICPNewsDB.php
 * 主要接口:
 *      function update_db_fresh($NewsDB, $url)
 *          从给定$url的目录页开始链式更新新闻数据,
 *          若目录页某条新闻已存在于数据库中则停止更新
 *      function update_db_all($NewsDB, $url)
 *          从给定$url的目录页开始链式更新新闻数据,
 *          从第一个目录页遍历只最后一个目录页
 */

// 依赖模块
include 'hydra_DICPNews.php';
include 'hydra_DICPNewsNav.php';
include 'hydra_DICPNewsDB.php';

// 各新闻栏目目录首页
$tzgg = "http://www.dicp.ac.cn/xwzx/tzgg/index.html";   // 通知公告
$xshd = "http://www.dicp.ac.cn/xwzx/xshd/index.html";   // 学术活动
$kjdt = "http://www.dicp.ac.cn/xwzx/kjdt/index.html";   // 科技动态
$zhxw = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";   // 综合新闻
$xsbg = "http://www.dicp.ac.cn/xwzx/xstz/xsbg/index.html";
$lwdb = "http://www.dicp.ac.cn/xwzx/xstz/lwdb/index.html";
// 各新闻栏目目录首页
$categ = array(
        'tzgg' => $tzgg, 
        'xshd' => $xshd, 
        'kjdt' => $kjdt, 
        'zhxw' => $zhxw,
	'xstz/xsbg' => $xsbg,
	'xstz/lwdb' => $lwdb
        );


function prepare_db($NewsDB)
// 功能: 准备新闻数据库
// 参数: $NewsDB, hydra_DICPNewsDB.php中定义的NewsDB类型对象;
// 返回: true, 成功; false, 失败;
{
    $NewsDB->err = 0;   // 清除异常状态
    
    $NewsDB->connect(); // 链接
    $NewsDB->init_db(); // 初始化数据库
    $NewsDB->init_tbs();    // 初始化数据表
    
    if (! $NewsDB->err)
    {// 初始化成功
        return true;
    }
    else 
    {// 初始化失败
        return false;
    }
}// function prepare_db()


function update_db_fresh($NewsDB, $url)
// 功能: 从给定$url的目录页开始链式更新新闻数据库, 若目录页某条新闻已存在于数据库中则停止更新;
// 参数: $NewsDB, 新闻存储数据库; $url, 新闻目录首页地址;
// 返回: true, 成功; false, 失败;
{
    // 初始化新闻目录页
    $nav = new NewsNavPage($url);
    $nav->get_all();
    
    // is fresh
    // 当前目录是否为新鲜目录(即是否存在新鲜新闻)
    $is_fresh = true;
    
    while ($nav)
    {// 新闻目录地址不为空
        $cnt_list = count($nav->news_list);
       // for ($i = 0; $i < $cnt_list; $i++)
	   for ($i = $cnt_list - 1; $i >= 0; $i--)
        {
            $news_url = $nav->news_list[$i]['href'];
            // 获取新闻内容
            $news = new News($news_url);
            $news->get_all();
            // 判断新闻条目是否存在于数据库
            if ($NewsDB->news_of_index($news->index))
            {// 存在于数据库
                $is_fresh = false; // 当前目录已非新鲜目录
                break;
            }
            else 
            {// 不存在于数据库
                // 存入数据库
                $NewsDB->insert_news($news);
            }
        }// for
        
        if ($is_fresh && $nav->next_url)
        {// 当前目录页为新鲜目录, 且存在`下一页`目录
            // 获取`下一页`
            $nav = NewsNavPage($nav->next_url);
            $nav->get_all();
        }
        else 
        {// 当前目录非新鲜目录或不存在`下一页`, 将是遍历循环终止
            $nav = false;
        }
    }// while
    
    return true;
}// function update_db_fresh()


function update_db_all($NewsDB, $url)
// 功能: 从给定$url的目录页开始链式更新新闻数据库, 从第一个目录页遍历只最后一个目录页;
// 参数: $NewsDB, 新闻存储数据库; $url, 新闻目录首页地址;
// 返回: true, 成功; false, 失败;
{
    // 初始化新闻目录页
    $nav = new NewsNavPage($url);
    $nav->get_all();
    while ($nav)
    {// 目录页不为空
        $cnt_list = count($nav->news_list);
        for ($i = 0; $i < $cnt_list; $i++)
        {
	    $new_datenew = $nav->news_list[$i]['time'];
            $news_urla = $nav->news_list[$i]['href'];
	    //echo $nav->news_list[$i]['time'];
            // 获取新闻内容
	    //echo $new_datenew;
	    
            $news = new News($news_urla);
	    $news->setDate($new_datenew);
            $news->get_all();
            // DEBUG MODE
            //print_r($news->src_url); echo "<br/>";
            //print_r($news->content_t); echo "<br/>";
            //echo $news->date_std;
	    //echo $news->index."index";
	    //echo $new_datenew."datenew";
            if ($NewsDB->news_of_index($news->index))
            {// 当前新闻存在于数据库
                // 继续循环
		break;
		//echo "continue"."<br>";
            }
            else
            {// 当前新闻不存在于数据库
                // 插入至数据库
                $NewsDB->insert_news($news);
            }
        }// for

        if ($nav->next_url)
        {// 当前目录页存在`下一页`
            // 获取`下一页`目录
            $nav = new NewsNavPage($nav->next_url);
            $nav->get_all();
        }
        else
        {// 不存在`下一页`目录
            $nav = false;
        }
    }// while
    
    return true;
}





?>
