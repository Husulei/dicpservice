<?php
/*
 * 模块: update_newsdb_all.php
 * 功能: 更新左右栏目新闻数据库
 *      从第一个目录页遍历只最后一个目录页
 */
include 'hydra_DICPNewsMng.php';

$tzgg = "http://www.dicp.ac.cn/xwzx/tzgg/index.html";   // ?..?..
$xshd = "http://www.dicp.ac.cn/xwzx/xshd/index.html";   // �?.活�.
$kjdt = "http://www.dicp.ac.cn/xwzx/kjdt/index.html";   // �..?��
$zhxw = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";   // 综�.?��.
$xsbg = "http://www.dicp.ac.cn/xwzx/xstz/xsbg/index.html";
$lwdb = "http://www.dicp.ac.cn/xwzx/xstz/lwdb/index.html";
// ?..?��.?..�.?�|5
$categ = array(
        'tzgg' => $tzgg,
        'xshd' => $xshd,
        'kjdt' => $kjdt,
        'zhxw' => $zhxw,
        'xstz/xsbg' => $xsbg,
        'xstz/lwdb' => $lwdb
        );
// 创建数据库管理类型对象
$ndb = new NewsDB("localhost", "root", "dicp*mobile", "trial_db");
// 准备数据库
prepare_db($ndb);
// 更新新所有栏目新闻
update_db_all($ndb, $xsbg);

?>
