<?php
include 'hydra_DICPNews.php';
include 'hydra_DICPNewsNav.php';
include 'hydra_DICPNewsDB.php';


$tzgg = "http://www.dicp.ac.cn/xwzx/tzgg/index.html";
$xshd = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";
$kjdt = "http://www.dicp.ac.cn/xwzx/kjdt/index.html";
$zhxw = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";

$categ = array(
        'tzgg' => $tzgg, 
        'xshd' => $xshd, 
        'kjdt' => $kjdt, 
        'zhxw' => $zhxw
        );


function prepare_db($NewsDB)
{
    $NewsDB->err = 0;
    
    $NewsDB->connect();
    $NewsDB->init_db();
    $NewsDB->init_tbs();
    
    if (! $NewsDB->err)
    {
        return true;
    }
    else 
    {
        return false;
    }
}// function prepare_db()


function update_db_fresh($NewsDB, $url)
{
    $nav = new NewsNavPage($url);
    $nav->get_all();
    
    // is fresh
    $is_fresh = true;
    
    while ($nav)
    {// page
        $cnt_list = count($nav->news_list);
        for ($i = 0; $i < $cnt_list; $i++)
        {
            $news_url = $nav->news_list[$i]['href'];
            $news = new News($news_url);
            $news->get_all();
            if ($NewsDB->news_of_index($news->index))
            {
                $is_fresh = false;
                break;
            }
            else 
            {
                $NewsDB->insert_news($news);
            }
        }// for
        
        if ($is_fresh && $nav->next_url)
        {
            $nav = NewsNavPage($nav->next_url);
            $nav->get_all();
        }
        else 
        {
            $nav = false;
        }
    }// while
    
    return true;
}// function update_db_fresh()


function update_db_all($NewsDB, $url)
{
    $nav = new NewsNavPage($url);
    $nav->get_all();
    
    while ($nav)
    {// page
        
        $cnt_list = count($nav->news_list);
        for ($i = 0; $i < $cnt_list; $i++)
        {
            $news_url = $nav->news_list[$i]['href'];
            $news = new News($news_url);
            
            // DEBUG MODE
            print_r($news->src_url); echo "<br/>";

            $news->get_all();

            // DEBUG MODE
            print_r($news->content_t); echo "<br/>";
            
            if ($NewsDB->news_of_index($news->index))
            {
                continue;
            }
            else
            {
                $NewsDB->insert_news($news);
            }
        }// for

        if ($nav->next_url)
        {
            $nav = new NewsNavPage($nav->next_url);
            $nav->get_all();
        }
        else
        {
            $nav = false;
        }
    }// while
    return true;
}





?>
