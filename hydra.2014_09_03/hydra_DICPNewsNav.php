<?php

class NewsNavPage
{
    var $url = "";
    var $url_base = "";
    var $url_dir = "";
    
    var $pre_url = "";
    var $next_url = "";
    
    var $news_list = "";
    
    var $dom = "";
    
    // exception info
    var $ok = true;
    var $err = 0;
    var $warn = 0;
    
    function NewsNavPage($url)
    {
        $url = strtolower( trim($url) );
        
        $url_pattern = '/^http:\/\//';
        
        if (preg_match($url_pattern, $url))
        {
            $this->url = $url;
            $this->url_base = basename($url);
            $this->url_dir = dirname($url);
            
            $this->ok = true;
        }
        else 
        {
            $this->ok = false;
            $this->err = 1;
        }
        
        return $this;
    }// function NewsNavPage()
    
    
    function get_all()
    {
        $this->init_dom();
        $this->get_news_list();
        $this->get_neighbour();
        
        return $this;
    }// function get_all()
    
    
    function init_dom()
    {
        $this->dom = new DOMDocument;
        $status = @$this->dom->loadHTMLFile($this->url);
        if (! $status)
        {
            $this->ok = false;
            $this->err = 2;
        }
        else
        {
            @$this->dom->normalizeDocument();
            $this->ok = true;
        }
        
        return $this;
    }// function init_dom()
    
    
    function get_news_list()
    {
        $finder = new DOMXPath($this->dom);
        $query_news = '//td[@class="hh14"]/a[@class="hh14"]';
        
        $result = $finder->query($query_news);
        
        $a_list = array();
        if ($result)
        {   
            for ($i = 0; $i < $result->length; $i++)
            {
                $a = $result->item($i);
                $href = $a->getAttribute('href');
                $href_abs = $this->url_dir . '/' . $href;
                $href_std = $this->url_remove_dot($href_abs);
                $a_list[$i]['href'] = $href_std;
                $a_list[$i]['value'] = $a->nodeValue;
            }
        }
        
        $this->news_list = $a_list;
        
        return $this;
    }// get_news_list()
    
    
    function get_neighbour()
    {
        $finder = new DOMXPath($this->dom);
        $pre = '//td/a[text()="上一页"]';
        $next = '//td/a[text()="下一页"]';
        
        // pre
        $result = $finder->query($pre);
        if ($result && $result->length > 0)
        {
            $href = $result->item(0)->getAttribute('href');
            $this->pre_url = $this->url_dir . '/' . basename($href);            
        }
        
        // next
        $result = $finder->query($next);
        if ($result && $result->length > 0)
        {
            $href = $result->item(0)->getAttribute('href');
            $this->next_url = $this->url_dir . '/' . basename($href);
        }
        
        return $this;
    }// function get_neighbour()
    
    
    function url_remove_dot( $path )
    {// from:
     // http://nadeausoftware.com/articles/2008/05/php_tip_how_convert_relative_url_absolute_url
        // multi-byte character explode
        $url_pattern = '/^http:\/\//';
        $base = basename($path);
        $path = dirname($path);
        $path = substr($path, 6, strlen($path) - 6);
        
        $inSegs  = preg_split( '!/!u', $path );
        $outSegs = array( );
        foreach ( $inSegs as $seg )
        {
            if ( $seg == '' || $seg == '.')
                continue;
            if ( $seg == '..' )
                array_pop( $outSegs );
            else
                array_push( $outSegs, $seg );
        }
        $outPath = implode( '/', $outSegs );
        /*
        if ( $path[0] == '/' )
            $outPath = '/' . $outPath;
        // compare last multi-byte character against '/'
        if ( $outPath != '/' &&
                (mb_strlen($path)-1) == mb_strrpos( $path, '/', 'UTF-8' ) )
                    $outPath .= '/';
         */
        $outPath = "http://" . $outPath . "/" . $base;
        
        return $outPath;
    }
    
}//NewsNavPage

?>