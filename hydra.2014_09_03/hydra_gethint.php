<?php
/******************************************************************************
**********************************DEBUG MODE**********************************/
error_reporting(E_ALL);
ini_set("display_errors","On");
/*****************************************************************************/

header("Content-Type: text/html; charset=utf8");
include 'hydra_DICPNewsMng.php';

$ndb = new NewsDB("localhost", "root", "dicp*mobile", "trial_db");

//for debug: localhost
//$ndb = new NewsDB("localhost", "root", "", "trial_db");
prepare_db($ndb);

$tzgg = "http://www.dicp.ac.cn/xwzx/tzgg/index.html";
$xshd = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";
$kjdt = "http://www.dicp.ac.cn/xwzx/kjdt/index.html";
$zhxw = "http://www.dicp.ac.cn/xwzx/zhxw/index.html";

$categ = array(
        'tzgg' => $tzgg,
        'xshd' => $xshd,
        'kjdt' => $kjdt,
        'zhxw' => $zhxw
        );

//$q=$_POST['q'];
// for debug, use $_GET
$q=$_GET['q'];

if (array_key_exists($q, $categ))
{
    $result = $ndb->news_of_categ($q);
    //print_r($result);
    for ($i = 0; $i < count($result); $i++)
    {        
        echo $result[$i]['ntitle'] . "*";
        echo $result[$i]['nauth'] . "*";
        echo $result[$i]['ntime'] . "*";
        echo $result[$i]['nurl'] . "*";
        echo $result[$i]['nindex'] . "$";
        
        // for debug:
        echo "\n";
    }
}

?>
