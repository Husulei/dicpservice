<?php
function say($x_value, $x_name)
{//$x_name is a string
    $content = $x_name . '::' . $x_value;
    echo '<p class="hydra_say" id="hydra_say">'.$content.'</p>';
    return TRUE;
}//say()


class POPAccount
{
    var $user = "";     //name@xxx.xxx
    var $passwd = "";   //password
    var $host = "";     //pop3 host
    var $port = "";     //pop3 port
    var $valid = FALSE; //account validation status
    var $pop_opt = "";  //Optional flags for $mailbox of `imap_open()'
    var $pop_path = ""; //first argument of `imap_open()'
    var $pop_mbox = ""; //imap stream opened by `imap_open()'

    function POPAccount($user, $passwd, $host, $port, $pop_opt="")
    {
        $this->user = $user;
        $this->passwd = $passwd;
        $this->host = $host;
        $this->port = $port;
        $this->pop_opt = $pop_opt;
        $this->pop_path = "{" . $host . ":" . $port . "/pop3" . $pop_opt . "}" . "INBOX";

        return TRUE;
    }//POPAccount()

    function open()
    {
        $this->mbox = @imap_open($this->pop_path, $this->user, $this->passwd);

        if (!$this->mbox)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }//open()

    function close()
    {
        if (!$this->mbox)
        {
            $status = @imap_close($this->mbox);
            $this->mbox = "";
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }//close()

    function validate()
    {//set validation tag
        if(!$this->mbox)
        {
            $this->valid = $this->open();
            $this->close();
        }
        else
        {
            $this->valid = TRUE;
        }

        return $this->valid;
    }//validate()

    
    function says()
    {//print the status for debug
        say($this->user, '$this->user');
        say(md5($this->passwd), '$this->passwd');
        say($this->host, '$this->host');
        say($this->port, '$this->port');
        say($this->valid, '$this->valid');
        say($this->pop_opt, '$this->pop_opt');
        say($this->pop_path, '$this->pop_path');
    }//says()


}//class POPAccount

?>