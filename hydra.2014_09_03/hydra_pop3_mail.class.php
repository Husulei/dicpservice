<?php
/*
 * Purpose: receive email via POP3
*/

function say($x_value, $x_name)
{//$x_name is a string
/*  $eval_exp = 'global ' . $x_name . ';';
    eval($eval_exp);
    
    $eval_exp = '$x_value = ' . $x_name . ';';
    eval($eval_exp);
*/    
    $content = $x_name . '::' . $x_value;
    echo '<p class="hydra_say" id="hydra_say">'.$content.'</p>';
    
    return TRUE;
}//say()

class POPAccount
{
    var $user = "";     //name@xxx.xxx
    var $passwd = "";   //password
    var $host = "";     //pop3 host
    var $port = "";     //pop3 port
    var $valid = FALSE; //account validation status
    var $pop_opt = "";  //Optional flags for $mailbox of `imap_open()'
    var $pop_path = ""; //first argument of `imap_open()'
    var $pop_mbox = ""; //imap stream opened by `imap_open()'

    function POPAccount($user, $passwd, $host, $port, $pop_opt="")
    {
        $this->user = $user;
        $this->passwd = $passwd;
        $this->host = $host;
        $this->port = $port;
        $this->pop_opt = $pop_opt;
        $this->pop_path = "{" . $host . ":" . $port . "/pop3" . $pop_opt . "}" . "INBOX";
        
        return TRUE;
    }//POPAccount()

    function open()
    {
        $this->mbox = @imap_open($this->pop_path, $this->user, $this->passwd);
        
        if (!$this->mbox)
        {
            return FALSE;             
        }
        else
        {
            return TRUE;
        }
    }//open()
    
    function close()
    {
        if (!$this->mbox)
        {
            $status = @imap_close($this->mbox);
            $this->mbox = "";
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }//close()
    
    function validate()
    {//set validation tag
        if(!$this->mbox)
        {
            $this->valid = $this->open();
            $this->close();
        }
        else
        {
            $this->valid = TRUE;
        }
        
        return $this->valid;
    }//validate()

    function says()
    {//print the status for debug
        say($this->user, '$this->user');
        say(md5($this->passwd), '$this->passwd');
        say($this->host, '$this->host');
        say($this->port, '$this->port');
        say($this->valid, '$this->valid');
        say($this->pop_opt, '$this->pop_opt');
        say($this->pop_path, '$this->pop_path');
    }//says()
    
    
}//class POPAccount


/******************************************************************************/
class POPMail
{
    var $account = "";
    var $mbox = "";

    function POPMail($account)
    {
        $this->account = new POPAccount($account->user, 
                                        $account->passwd, 
                                        $account->host, 
                                        $account->port, 
                                        $account->pop_opt);


        $this->mbox = & $this->account->mbox;
        
        $open = $this->account->open();
        
        return $open;
    }//POPMail()

    function reopen()
    {   
        if(!$this->mbox)
        {
            return $this->account->open();
        }
        else
        {
            return TRUE;
        }        
    }//reopen()
    
    function header_t($i)
    {//Returns the header of the specified message as a text string.
        return imap_fetchheader($this->mbox, $i);
    }//header_t

    function header($i)
    {//Returns the information in an object
        //make sure: $i >= 1
        return imap_headerinfo($this->mbox, $i);
    }//header()

    function header_field($i, $field)
    {//Return a certain field of a header, maybe is a MIME element
        $header = $this->header($i);
        $ret = "";
        $eval = "\$ret = \$header->" . $field . ";";
        //$field maybe invalid, the caller could check the $ret if it cares
        @eval($eval);
        return $ret;
    }//header_field()

    function header_field_t($i, $field)
    {//Return a certain field of a header, as a readable string
        $field_m = $this->header_field($i, $field);
        $field_t = $this->mime_dec_t($field_m);
        //$field_t = $this->iconv_utf8($field_x);
        return $field_t;
    }//header_field_t()

    function mime_dec_t($text)
    {//Decode MIME header elements, return as a single STRING
        $parts = imap_mime_header_decode($text);
        $part_i = "";
        $ret = "";
        for ($i=0; $i<count($parts); $i++)
        {
            if ($parts[$i]->charset != "utf-8" && $parts[$i]->charset != "default")
            {
                $part_i = iconv($parts[$i]->charset, "utf-8", $parts[$i]->text);
            }
            $ret = $ret . $part_i;
        }
        return $ret;
    }//mime_dec_t()

    /*
     function match_gb($text)
     {
    $t = $text;
    if (ucwords(substr($t, 0, 2)) == "GB")
    {
    return 1;
    }
    else
    {
    return 0;
    }
    }

    function iconv_utf8($text)
    {
    $s1 = iconv('gbk', 'utf-8', $text);
    $s0 = iconv('utf-8', 'gbk', $s1);
    if($s0 == $text)
    {
    return $s1;
    }
    else
    {
    return $text;
    }
    }
    */


    function counts()
    {
        $headers = imap_headers($this->mbox);
        return count($headers);
    }

}//end POPMail


?>