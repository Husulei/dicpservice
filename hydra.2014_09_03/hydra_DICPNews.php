<?php

class News
{
    var $src_url = "";
    var $src_base = "";
    var $src_dir = "";
    
    var $dom = "";
    
    // mata datas
    var $categ_t = "";
    var $categ_n = "";
    
    var $index = "";
    var $date = "";
    var $date_std = "";
    
    var $title = "";
    var $department = "";
    
    var $content_dom = "";
    //var $content_div = "";
    var $content_t = "";
    
    var $images = "";
    var $images_t = "";
    var $attachs = "";
    
    // category of news
    var $categ2n = array(
            'zhxw' => 11,
            'kjdt' => 12,
            'xshd' => 13,
            'tzgg' => 14
            );
    var $n2categ = array(
            11 => 'zhxw',
            12 => 'kjdt',
            13 => 'xshd',
            14 => 'tzgg'
            );
    
    // reserved tag in content of news
    var $reserved = array(
            'tag' => array('div', 'p', 'a'),
            'attr' => array('href', 'src')
            );
    
    // debug info
    var $ok = true;
    var $err = 0;
    var $warn = 0;
    var $n = 0;
    
    function News($url)
    {
        $url = strtolower( trim($url) );
        
        $url_pattern = '/^http:\/\//';        
        if (preg_match($url_pattern, $url))
        {
            $this->src_url = $url;
            $this->src_base = basename($url);
            $this->src_dir = dirname($url);
            
            $this->ok = true;
        }
        else 
        {
            $this->ok = false;
            $this->err = 1;
        }
        
        return $this;
    }// function News()
    
    
    function get_all()
    {
        $this->matainfo();
        $this->init_dom();
        $this->get_title();
        $this->get_department();
        $this->get_content_dom();
        $this->clean_content_dom();
        $this->list_img();
        $this->get_content_t();
        
        return $this;
    }// function get_all();
    
    
    function matainfo()
    {
        // parse full url into seperate directory level
        $path = substr($this->src_url, 7);
        $path_a = explode('/', $path);
        $depth = count($path_a);
        
        // get category
        $categ = $path_a[2];
        //! for nesting category 
        //! defined as "xstz/xsbg"
        $categ_pattern = '/^[a-z]+$/';
        for ($i = 3; $i < $depth; $i++)
        {
            if (preg_match($categ_pattern, $path_a[$i]))
            {
                $categ = $categ . '/' . $path_a[$i];
            }
            else 
            {
                break;
            }
        }
        // category in text
        $this->categ_t = $categ;
        // category in enumeration
        if (array_key_exists($categ, $this->categ2n))
        {
            $this->categ_n = $this->categ2n[$categ];
        }
        else
        {// category DO NOT included in current consideration
            $this->categ_n = 99;
            $this->warn = 1;
        }
        
        // get *.html file name
        $htm = $path_a[$depth - 1];
        $basename = substr($htm, 1, strlen($htm) - 6);
        $basename_a = explode('_', $basename);
        
        // get date
        $date_text = $basename_a[0];
        $date_format = "%Y%m%d";
        $date['Y'] = substr($date_text, 0, 4);
        $date['m'] = substr($date_text, 4, 2);
        $date['d'] = substr($date_text, 6, 2);
        $this->date = $date;
        $this->date_std = date("Y-m-d", mktime(0, 0, 0, $date['m'], $date['d'], $date['Y']));
        // generate index
        $this->index = "$this->categ_n" . '_' . $basename;
        
        return $this;
    }// function matainfo()
    
    function init_dom()
    {
        $this->dom = new DOMDocument;
        $status = @$this->dom->loadHTMLFile($this->src_url);
        if (! $status)
        {
            $this->ok = false;
            $this->err = 2;
        }
        else 
        {
            @$this->dom->normalizeDocument();
            $this->ok = true;
        }
        
        return $this;
    }//function init_dom()
    
    
    function get_title()
    {// use XPath
        $finder = new DOMXPath($this->dom);
        $query_title = '//table[@class = "blu23l28"]/tbody/tr/td[@width = "100%"]';
        $title_xml = $finder->query($query_title)->item(0);
        
        $this->title = $title_xml->textContent;
        
        // trim useless white space
        trim($this->title);
        
        return $this;
    }// function get_title()
    
    
    function set_title($title)
    {// possiblely be used before fetch and parse *.htm file
        $this->title = $title;
        return $this;
    }// function set_title()
    

    function get_department()
    {// use XPath
        $finder = new DOMXPath($this->dom);
        $query_depart = '//span[@id = "auth"]';
        
        $depart_anchor = $finder->query($query_depart);
        if ($depart_anchor->length > 0)
        {
            $depart_anchor = $finder->query($query_depart)->item(0);
            $depart_name = $depart_anchor->nextSibling;
            if ($depart_name)
            {
                $this->department = $depart_name->data;
                trim($this->department);
            }
        }
        
        return $this;
    }// function get_department()
    
    
    function get_content_dom()
    {// XPath query
        $finder = new DOMXPath($this->dom);
        
        //$query_content = '//div[@class = "TRS_Editor"][p or font or div or span]';
        //$query_content = '//div[@class = "TRS_Editor" or @class = "cas_content"]'
        //$query_content = '//td[@class = "h14124"][p]';
        $query = array(
                '//div[@class = "TRS_Editor" or @class = "cas_content"][p]',
                '//*[p]',
                '//div[@class = "TRS_Editor" or @class = "cas_content"]'
                );
        
/***************************************************************/
        for ($i = 0; $i < count($query); $i++)
        {
            $result = $finder->query($query[$i]);
            if ($result->length > 0)
            {
                $content = $result->item(0);
                $this->content_dom = new DOMDocument;
                $this->content_dom->loadXML($content->C14N());
                
                return $this;
            }            
        }
/**************************************************************/
/***************************************************************
        for ($i = 0; $i < count($query); $i++)
        {
            $result = $finder->query($query[$i]);

            $content = "";
            for ($j = 0; $j < $result->length; $j++)
            {
                $content .= $result->item($j)->C14N();
            }
            
            if ($content)
            {
                $this->content_dom = new DOMDocument;
                $this->content_dom->loadXML($content);
                return $this;
            }
        }
**************************************************************/
        
        $this->err = 11;
        return $this;
        /* discarding method
        $paras = '';
        foreach ($content->getElementsByTagName('p') as $p)
        {
            $p->removeAttribute('align');
            $p->removeAttribute('style');
            $paras = $paras . $p->C14N() . "\n";
        }
        */
        
    }// function get_content()
    
    
    function clean_content_dom()
    {
        //$this->do_clean_imgsrc($this->content_dom);
        //$this->do_clean_ahref($this->content_dom);
        $this->do_clean_hyperlink($this->content_dom);
        $this->do_clean_node($this->content_dom);
        
        return $this;
    }
    
    
    function get_content_t()
    {
        $this->content_t = '';
        
        $children = $this->content_dom->childNodes;
        for ($i = 0; $i < $children->length; $i++)
        {
            $this->content_t = $this->content_t . $children->item($i)->C14N();
        }
        
        return $this;
    }// function get_content_t()
    

    function do_clean_node($node)
    {
        /* maxmum depth for recursive call
         * for test use
        if ($this->n > 20)
        {
            return false;
        }
        */
        
        if (! $this->is_nest($node))
        {
            if (! $this->is_text($node))
            {
                $this->do_clean_top($node);
            }
            
            return true;
        }
        else 
        {
            //$this->n ++;
            
            $children = $node->childNodes;
            for ($i = 0; $i < $children->length; $i++)
            {
                $this->do_clean_node($children->item($i));
            }
            
            //$this->n --;
            $this->do_clean_top($node);
            return true;
        }
    }

    
    function do_clean_top($node)
    {
        $info = $this->is_reserved($node);
        if (! $info)
        {// 
            $this->do_expand_top($node);
            return true;
        }
        else
        {//
            $this->do_renorm_top($node, $info);
            return true;
        }
    }
    
    
    function do_expand_top($node)
    {
        // in case for root node
        // becasue insertBefore() is invalid
        $parent = $node->parentNode;
        if (! $parent)
        {
            return true;
        }
                
        $children = $node->childNodes;
        for ($i = 0; $i < $children->length; $i++)
        {
            $child = $children->item($i)->cloneNode(true);
            $parent->insertBefore($child, $node);
        }
        
        $parent->removeChild($node);
        
        return true;
    }// function do_clean_top()
    
    
    function do_renorm_top($node, $info)
    {
        // in case for root node
        // becase appendChild() is invalid
        $parent = $node->parentNode;
        if (! $parent)
        {
            return true;
        }
        
        if($info)
        {
            $root = $node->ownerDocument;
            $newnode = $root->createElement($info['name']);
            if(count($info['attr'])>0)
            {
                foreach (array_keys($info['attr']) as $attr_name)
                {
                    $attr_value = $info['attr'][$attr_name];
                    $newnode->setAttribute($attr_name, $attr_value);
                }
            }
            
            $children = $node->childNodes;
            for ($i = 0; $i < $children->length; $i++)
            {
                $child = $children->item($i)->cloneNode(true);
                $newnode->appendChild($child);
            }
            
            $parent->insertBefore($newnode, $node);
            $parent->removeChild($node);
            
            return true;
        }
        
    }// function do_renorm_top()

    
    function is_nest($node)
    {
        if ($node->hasChildNodes())
        {
            foreach ($node->childNodes as $child)
            {
                if ($child->nodeType == XML_ELEMENT_NODE)
                {
                    return true;
                }
            }
        }
        else 
        {
            return false;
        }
    }// function is_nest();
    
    
    function is_text($node)
    {
        if ($node->nodeType == XML_TEXT_NODE)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }// function is_text()
    
    
    function is_reserved($node)
    {
        // in case for root node
        // because getAttribute() is invalid
        if ($node->nodeType == XML_DOCUMENT_NODE)
        {
            return false;
        }
        
        $nodeName = "";
        $nodeAttr = array();
        $result = array();
        
        foreach ($this->reserved['attr'] as $attr_name)
        {
            if ($attr_value = $node->getAttribute($attr_name))
            {
                $nodeAttr[$attr_name] = $attr_value;
            }
        }
        
        $is_rsrv_attr = count($nodeAttr);
        $is_rsrv_tag = in_array($node->nodeName, $this->reserved['tag']);
        if ($is_rsrv_attr || $is_rsrv_tag)
        {
            $nodeName = $node->nodeName;
            $result = array(
                'name' => $nodeName,
                'attr' => $nodeAttr
                );

            return $result;
        }
        else 
        {
            return false;
        }
    }// function is_reserved()
    
    
    function do_clean_hyperlink($dom)
    {// a uniform method to clean all possible kind of hyperlinks
     // and extensible
        $link_types = array(
                        'src',
                        'href'
                        );
        
        $finder = new DOMXPath($dom);
        
        foreach ($link_types as $type)
        {
            $query = '//*[@' . $type . ']';
            
            $links = $finder->query($query);
            
            for ($i = 0; $i < $links->length; $i++)
            {
                $link = $links->item($i);
                $src = $link->getAttribute($type);
                $src_abs = $this->do_format_url($src);
                $link->setAttribute($type, $src_abs);
            }
        }
        return true;
    }
    
    
    // too specific
    function do_clean_imgsrc($dom)
    {
        $finder = new DOMXPath($dom);
        $query_img = '//img';
        
        $imgs = $finder->query($query_img);
        
        for ($i = 0; $i < $imgs->length; $i++)
        {
            $img = $imgs->item($i);
            $src = $img->getAttribute("src");
            $src_abs = $this->do_format_url($src);
            $img->setAttribute("src", $src_abs);
        }
        
        return true;
    }// function do_clean_imgsrc()
    
    
    // too specific
    function do_clean_ahref($dom)
    {
        $finder = new DOMXPath($dom);
        $query_a = '//a';
        
        $anchors = $finder->query($query_a);
        
        for ($i = 0; $i < $anchors->length; $i++)
        {
            $a = $anchors->item($i);
            $href = $a->getAttribute("href");
            $href_abs = $this->do_format_url($href);
            $a->setAttribute('href', $href_abs);
        }
        
        return true;
    }// function do_clean_ahref()
    
    
    function do_format_url($url)
    {
        if (! $this->is_absurl($url))
        {
            $url = $this->src_dir . '/' . $url;
        }
        
        return $url;
    }// function do_format_url();
    
    
    function is_absurl($url)
    {
        $url = strtolower( trim($url) );
        
        $url_pattern = '/^http:\/\//';
        if (preg_match($url_pattern, $url))
        {
            return true;
        }
        else
        {
            return false;
        }        
    }// function is_absurl()
    
    
    function list_img()
    {
        $finder = new DOMXPath($this->content_dom);
        $query_img = '//img';
        
        $imgnodes = $finder->query($query_img);
        $imgs = array();
        $this->images_t = '';
        for ($i = 0; $i < $imgnodes->length; $i++)
        {
            $imgs[$i] = $imgnodes->item($i)->getAttribute('src');
            $this->images_t = $this->images_t . "<src>" . $imgs[$i] . "</src>";
        }
        
        $this->images = $imgs;
        $this->images_t = $this->images_t;
        return $this;
    }
    
    /* TODO
        function fetch_attach()
    {
        return $this;
    }
    
    
    */
    
    
    function clear_err()
    {
        $this->ok = true;
        $this->err = 0;
        $this->warn = 0;
        
        return $this;
    }
}


?>
