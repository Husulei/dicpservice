<?php
include 'hydra_fecho.php';

date_default_timezone_set("Asia/Shanghai");
$time = date("Y-m-d h:i:s", time());

function xtitle($title)
{
    $X = str_repeat('-', 40);
    return $X . $title . $X;
}

$STAMP = xtitle($time);

//echo date_default_timezone_get() . "<br/>";
echo $STAMP . "<br/>";

$FEcho->xecho($STAMP)->endl();

/***************************************************/
$url_case_01 = "http://www.dicp.ac.cn/xwzx/tzgg/201407/t20140728_4168251.html";

$htm_case_01 = new DOMDocument;
@$htm_case_01->loadHTMLFile($url_case_01);

$obj_case_01 = print_r($htm_case_01, true);
//$FEcho->xecho($obj_case_01);

$FEcho->xecho($STAMP)->endl();

$finder = new DOMXPath($htm_case_01);

$FEcho->xecho(xtitle("Content"))->endl();
$query_content = '//div[@class = "TRS_Editor"]';
$content = $finder->query($query_content)->item(0);
$content_str = $content->C14N();
$FEcho->xecho($content_str)->endl();
//$FEcho->xecho($content->textContent)->endl();

$paras = '';
foreach ($content->getElementsByTagName('p') as $p)
{
    $p->removeAttribute('align');
    $paras = $paras . $p->C14N() . "\n";
}


$FEcho->xecho($paras)->endl();
//$FEcho->xecho($content->C14N())->endl();
//$FEcho->xecho(print_r($content, true));



?>