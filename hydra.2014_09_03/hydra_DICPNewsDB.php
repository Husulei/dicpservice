<?php

class NewsDB
{

// mysql account
    var $server = "localhost";
    var $user = "root";
    var $passwd = "";
    
    // db connect
    var $Conn = "";
    
    // working db
    var $DB = "";
    
    
    // exception info
    var $ok = true;
    var $err = 0;
    var $warn = 0;
    
    function NewsDB($s, $u, $p, $db)
    {
        $this->server = $s;
        $this->user = $u;
        $this->passwd = $p;
        $this->DB = $db;
    }// function NewsDB()
    
    function connect()
    {
        $this->Conn = new mysqli($this->server, $this->user, $this->passwd);
        
        if (! $this->Conn)
        {
            $this->err = 1;
            return false;
        }
        else
        {
            $this->Conn->autocommit(true);
            $this->Conn->set_charset("utf8");
            //$this->Conn->query("set character set 'utf8'");
            $this->Conn->query("set names 'utf8'");
            return true;
        }
    }// function connect()
    
    
    function disconnect()
    {
        $this->Conn->close();
        return $this;
    }
    
    
    function do_create_db($db)
    {
        $sql = "CREATE DATABASE " . $db;
        
        $status = $this->Conn->query($sql);
        
        if ($status)
        {
            return true;
        }
        else
        {
            return false;
        }
    }// function do_create_db()
    
    
    function do_db_exist($db)
    {
        $sql = 'SHOW DATABASES LIKE "' . $db . '"';
        $db_list = $this->Conn->query($sql);
        
        if ($db_list->num_rows > 0)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }// function do_db_exist()
    
    
    function init_db()
    {
        if (! $this->do_db_exist($this->DB))
        {
            $status = $this->do_create_db($this->DB);
            if (! $status)
            {
                $this->err = 21;
                return false;
            }
        }

        $this->Conn->select_db($this->DB);
        return true;
    }// function init_db()
    
    
    function do_tb_exist($tb)
    {
        $sql = 'SHOW TABLES LIKE "' . $tb . '"';
        $tb_list = $this->Conn->query($sql);
        
        if ($tb_list->num_rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }// function do_tb_exist($tb)
    
    
    function do_create_tb($query_tb)
    {
        $this->Conn->query($query_tb);
        $this->Conn->commit();
    }// function do_create_tb()
    
    
    function init_tbs()
    {
        // Stucture of news
        $sql_news = "
                CREATE TABLE news_tb
                (
                    nid         int AUTO_INCREMENT,
                    nindex      varchar(256)    UNIQUE,
                    ncateg      varchar(16),
                    nurl        varchar(256),
                    ntitle      varchar(256),
                    ntime       date,
                    nauth       varchar(256),
                    
                    PRIMARY KEY(nid)
                )
                ";
        
        $sql_content = "
                CREATE TABLE content_tb
                (
                    cid         int AUTO_INCREMENT,
                    nindex      varchar(256),
                    content     text,
                    nimgs       text,
                
                    PRIMARY KEY(cid)
                )
                ";
        
        /*
        $sql_pic = "
                CREATE TABLE pic_tb
                (
                    pic_id      int,
                    news_id     int,
                    pic_order   int,
                    pic_url     varchar(255),
                
                    PRIMARY KEY(pic_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        $sql_attach = "
                CREATE TABLE attach_tb
                (   
                    attach_id   int,
                    news_id     int,
                    attach_order int,
                    attach_url  varchar(255),
                
                    PRIMARY KEY(attach_id),
                    FOREIGN KEY(news_id) REFERENCES news_tb (news_id)
                )
                ";
        */
        
        $tbs = array(   
                "news_tb", 
                "content_tb",
                    );
        
        $sql = array(   
                $tbs[0] => $sql_news,
                $tbs[1] => $sql_content,
                    );
        
        $this->Conn->autocommit(false);
        for ($i = 0; $i < 2; $i++)
        {
            // test if TABLE exists
            if (! $this->do_tb_exist($tbs[$i]))
            {
                $this->Conn->query($sql[$tbs[$i]]);
            }
        }
        
        if (! $this->Conn->errno)
        {
            $this->Conn->commit();
            $this->Conn->autocommit(true);
            return true;
        }
        else 
        {
            $this->Conn->rollbak();
            $this->Conn->autocommit(true);

            $this->err = 22;
            return false;
        }        
        
    }//function do_init_tbs()
    
    
    function insert_news($news)
    {   
        // INSURE THAT: $news is an object (defined in 'hydra_DICPNews.php')
        if (! (gettype($news) == 'object' && get_class($news) == 'News') )
        {
            $this->err = 31;
            return false;
        }

        // escape special characters
        /**************************************************************
        $index  = $this->Conn->real_escape_string($news->index);
        $categ  = $this->Conn->real_escape_string($news->categ_t);
        $url    = $this->Conn->real_escape_string($news->src_url);
        $title  = $this->Conn->real_escape_string($news->title);
        $time   = $this->Conn->real_escape_string($news->date_std);
        $auth   = $this->Conn->real_escape_string($news->department);
        $cntnt  = $this->Conn->real_escape_string($news->content_t);
        $imgs   = $this->Conn->real_escape_string($news->images_t);
        ***************************************************************/
        
        $index  = $news->index;
        $categ  = $news->categ_t;
        $url    = $news->src_url;
        $title  = $news->title;
        $time   = $news->date_std;
        $auth   = $news->department;
        $cntnt  = $news->content_t;
        $imgs   = $news->images_t;
 
        // remove non-breaking space: do not work
        //$cntnt = str_replace("\xa0",'', $cntnt);

        // DEBUG MODE
        print_r($cntnt); echo "<br/>";

        $sql_news = "
                INSERT INTO news_tb 
                (nindex, ncateg, nurl, ntitle, ntime, nauth)
                VALUES
                ('$index', '$categ', '$url', '$title', '$time', '$auth')
                ";
        $sql_cntnt = "
                INSERT INTO content_tb
                (nindex, content, nimgs)
                VALUES
                ('$index', '$cntnt', '$imgs')
                ";
        // do insert
        $this->Conn->autocommit(false);
        $this->Conn->query($sql_news);
        $this->Conn->query($sql_cntnt);
        if (! $this->Conn->errno)
        {
            $this->Conn->commit();
            return true;
        }
        else
        {
            $this->Conn->rollback();
            $this->err = 32;
            return false;
        }
        
    }// function insert_news()
    
    
    function delete_news($index)
    {
        $sql_news = "
                DELETE FROM trial_db WHERE nindex='$index'
                ";
        $sql_cntnt = "
                DELETE FROM content_tb WHERE nindex='$index'
                ";
        
        /*
        $sql_news = "
                DELETE content_tb, trial_db
                FROM content_tb, trial_db 
                WHERE 
                    content_tb.nindex='$index'
                    AND
                    trial_db.nindex='$index'
                ";
        */
        $this->Conn->autocommit(false);
        $this->Conn->query($sql_cntnt);
        $this->Conn->query($sql_news);
        if (! $this->Conn->errno)
        {
            $this->Conn->commit();
            return true;
        }
        else
        {
            //$this->Conn->rollback();
            $this->err = 33;
            return false;
        }
        
    }// function delete_news()
    
    
    function news_of_index($index)
    {
        $sql = "SELECT * FROM news_tb WHERE nindex='$index'";
        $result = $this->Conn->query($sql);
        
        if ($result)
        {
            $row = $result->fetch_assoc();
            return $row;
        }
        else 
        {
            $this->err = 41;
            return false;
        }
    }// function news_of_index()
    
    
    function cntnt_of_index($index)
    {
        $sql = "SELECT * FROM content_tb WHERE nindex='$index'";
        $result = $this->Conn->query($sql);
        
        if ( $result )
        {
            $row = $result->fetch_assoc();
            return $row;
        }
        else 
        {
            $this->err = 41;
            return false;
        }
    }// function cntnt_of_index()
    
    
    function news_of_categ($categ)
    {
        $sql = "
            SELECT * FROM news_tb 
            WHERE ncateg='$categ'
            ORDER BY ntime DESC
            ";
        
        $result = $this->Conn->query($sql);
        if ( $result )
        {
            $news = array();
            $i = 0;
            while ($row = $result->fetch_assoc())
            {
                $news[$i] = $row;
                $i++;
            }
            return $news;
        }
        else
        {
            $this->err = 41;
            return false;
        }//if
    }// function news_of_categ()

}// class NewsDB

?>
